import fedmsg
import koji
from pymongo import MongoClient
from datetime import datetime as dt

class StatusApp:
    def __init__(self):
        host = 'localhost'
        port = 27017
        client = MongoClient(host, port)
        db_name = 'statusapp'
        db = client[db_name]
        collection_name = 'post_status'
        self.collection = db[collection_name]
        self.usr_names = {
            'anishpatil': 'anish',
            'tagoh': 'tagoh',
            'petersen': 'petersen',
            'pravins' : 'pravins',
            'fujiwara' : 'fujiwara',
            'pwu' : 'pwu',
            'ueno' : 'ueno',
            'mfabian' : 'mfabian',
            'paragn' : 'parag',
            'pnemade': 'parag',
            'shilpagite' : 'shilpagite'
        }

    def suffix(self,d):
        return 'th' if 11<=d<=13 else {1:'st',2:'nd',3:'rd'}.get(d%10, 'th')

    def custom_strftime(self,format, t):
        return t.strftime(format).replace('{S}', str(t.day) + self.suffix(t.day))

    def insert_status(self,user_name,msg):
        try:
            status = {
                'user':user_name,
                'status': msg,
                'date': self.custom_strftime('%B {S} %Y,', dt.now())
            }
            self.collection.insert(status)
        except:
            print "Exception in insert status",msg    

# Create global object
s = StatusApp()

# Fas usernames to be monitored
users = (
    'anishpatil',
    'tagoh',
    'petersen',
    'pravins',
    'fujiwara',
    'pwu',
    'ueno',
    'mfabian',
    'paragn',
    'pnemade',
    'shilpagite',
)

def process_new_bug(msg):
    bug_reports(msg,":New Bug ")

def process_new_bug_update(msg):
    bug_reports(msg,":Updated Bug ")

def bug_reports(msg,bug_status):
    try:
        user_name = msg['username']
        if user_name in users:
            summary = msg['summary']
            url = msg['weburl']
            status_msg =  bug_status + summary + " "+ url
            s.insert_status(user_name,status_msg)
            print user_name,status_msg
    except:
        print "Error in processing bug report", msg


def process_build(msg):
    try:
        user_name = msg['msg']['owner']
        if user_name in users:
            build_status = msg['msg']['new']
            if build_status == koji.BUILD_STATES['COMPLETE']:
                status_msg = msg['msg']['name'] +'-'+ msg['msg']['version'] +"-"+ msg['msg']['release'] +" built"  
                s.insert_status(user_name,status_msg)
                print user_name,status_msg
    except:
        print "Error in process build" + msg


def process_blog(msg):
    try:
        user_name = msg['msg']['username']
        if user_name in users:
            status_msg = " New Blog Post: " +  msg['msg']['post']['title'] + "url:" +  msg['msg']['post']['link']
            s.insert_status(user_name,status_msg)
            print user_name,status_msg
    except:
        print "Error in process blog" , msg



t_names = { 
    'bugzilla.bug.new': process_new_bug,
    'bugzilla.bug.update' : process_new_bug_update,
    'org.fedoraproject.prod.buildsys.build.state.change': process_build,
    'org.fedoraproject.prod.planet.post.new': process_blog
}

for name, endpoint, topic, msg in fedmsg.tail_messages():
    try:
        t_names[topic](msg)
    except KeyError:
        #No key exists that means i am not interested in handling that Topic
        continue
    except:		
         print "exception"
